class employee:
    no_of_leaves = 8

    def __init__(self,aname,asalary,arole):
            self.name = aname
            self.salary = asalary
            self.role = arole
#class method is used for change class variable through instance variable and also used alternative constructor
    @classmethod
    def change_leaves(cls,newleaves):        
            cls.no_of_leaves = newleaves



rohan = employee("rohan",466,"teacher")
ajay = employee("ajay",733,"dance")
rohan.change_leaves(45)
print(rohan.no_of_leaves)